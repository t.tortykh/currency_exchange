import 'package:currency_exchange/config/constants/injectable_environments.dart';
import 'package:currency_exchange/core/di/service_locator.dart';
import 'package:currency_exchange/data/services/local_storage_service.dart';
import 'package:currency_exchange/general_app/general_app_wrapper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

void main() async {
  await dotenv.load();

  await configureDependencies(CurrencyExchangeEnv.dev);

  await LocalStorageService.instance.init();

  runApp(const GeneralAppWrapper());
}
