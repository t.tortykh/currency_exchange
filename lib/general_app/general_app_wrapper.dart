import 'package:currency_exchange/features/user_settings/cubit/settings_cubit.dart';
import 'package:currency_exchange/general_app/general_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GeneralAppWrapper extends StatelessWidget {
  const GeneralAppWrapper({super.key});

  @override
  Widget build(BuildContext context) => MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => SettingsCubit(),
            lazy: false,
          ),
        ],
        child: const GeneralApp(),
      );
}
