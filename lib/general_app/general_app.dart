import 'package:currency_exchange/config/router/router.dart';
import 'package:currency_exchange/config/theme/app_theme.dart';
import 'package:currency_exchange/data/models/settings_model.dart';
import 'package:currency_exchange/extensions/theme_mode.dart';
import 'package:currency_exchange/features/user_settings/cubit/settings_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class GeneralApp extends StatelessWidget {
  const GeneralApp({super.key});

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<SettingsCubit, SettingsModel>(
        builder: (context, settings) => MaterialApp.router(
          title: 'Currency_exchange',
          debugShowCheckedModeBanner: false,
          theme: lightTheme,
          darkTheme: darkTheme,
          themeMode: ThemeModeFromString.modeFromString(settings.themeMode),
          localizationsDelegates: AppLocalizations.localizationsDelegates,
          supportedLocales: AppLocalizations.supportedLocales,
          locale: Locale(settings.locale),
          routerConfig: router,
        ),
      );
}
