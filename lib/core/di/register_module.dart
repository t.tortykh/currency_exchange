import 'package:currency_exchange/api/client/api_constants.dart';
import 'package:currency_exchange/api/client/currency_exchange_api_client.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

@module
abstract class RegisterModule {
  @singleton
  Dio get dio => Dio(BaseOptions(baseUrl: ApiConstants.baseDomain))
    ..options.sendTimeout = const Duration(milliseconds: 10000)
    ..options.receiveTimeout = const Duration(milliseconds: 10000)
    ..interceptors.add(
      PrettyDioLogger(requestHeader: true, requestBody: true),
    );

  @singleton
  CurrencyExchangeApiClient apiClient(Dio dio) =>
      CurrencyExchangeApiClient(dio);
}
