// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i3;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../../api/client/currency_exchange_api_client.dart' as _i4;
import '../../data/repositories/api/exchange_rate_repo.dart' as _i5;
import '../../features/exchange_rate/cubit/exchange_rate_cubit.dart' as _i6;
import 'register_module.dart' as _i7;

const String _dev = 'dev';

// initializes the registration of main-scope dependencies inside of GetIt
_i1.GetIt $configureDependencies(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  final registerModule = _$RegisterModule();
  gh.singleton<_i3.Dio>(() => registerModule.dio);
  gh.singleton<_i4.CurrencyExchangeApiClient>(
      () => registerModule.apiClient(gh<_i3.Dio>()));
  gh.factory<_i5.ExchangeRateRepo>(
    () => _i5.ExchangeRateRepoImpl(gh<_i4.CurrencyExchangeApiClient>()),
    registerFor: {_dev},
  );
  gh.singleton<_i6.ExchangeRateCubit>(
      () => _i6.ExchangeRateCubit(gh<_i5.ExchangeRateRepo>()));
  return getIt;
}

class _$RegisterModule extends _i7.RegisterModule {}
