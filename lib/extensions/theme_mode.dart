import 'package:flutter/material.dart';

extension ThemeModeFromString on ThemeMode {
  static ThemeMode modeFromString(String? mode) => switch (mode) {
        'light' => ThemeMode.light,
        'dark' => ThemeMode.dark,
        _ => ThemeMode.system
      };
}
