import 'package:currency_exchange/config/constants/app_icons.dart';
import 'package:currency_exchange/config/router/page_routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final locale = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(locale.appName),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () => context.go(AppNamedRoutes.exchangeRate),
          child: Text(locale.currencyToday),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        backgroundColor: theme.colorScheme.tertiary,
        onPressed: () => context.go(AppNamedRoutes.settings),
        child: SvgPicture.asset(
          AppIcons.settings,
        ),
      ),
    );
  }
}
