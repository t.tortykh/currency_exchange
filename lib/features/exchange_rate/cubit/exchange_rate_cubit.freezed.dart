// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'exchange_rate_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ExchangeRateState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<CurrencyEntity> currencyList) feched,
    required TResult Function() failed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<CurrencyEntity> currencyList)? feched,
    TResult? Function()? failed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<CurrencyEntity> currencyList)? feched,
    TResult Function()? failed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_LoadingExchangeRateState value) loading,
    required TResult Function(_FechedExchangeRateState value) feched,
    required TResult Function(_FailedExchangeRateState value) failed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_LoadingExchangeRateState value)? loading,
    TResult? Function(_FechedExchangeRateState value)? feched,
    TResult? Function(_FailedExchangeRateState value)? failed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_LoadingExchangeRateState value)? loading,
    TResult Function(_FechedExchangeRateState value)? feched,
    TResult Function(_FailedExchangeRateState value)? failed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExchangeRateStateCopyWith<$Res> {
  factory $ExchangeRateStateCopyWith(
          ExchangeRateState value, $Res Function(ExchangeRateState) then) =
      _$ExchangeRateStateCopyWithImpl<$Res, ExchangeRateState>;
}

/// @nodoc
class _$ExchangeRateStateCopyWithImpl<$Res, $Val extends ExchangeRateState>
    implements $ExchangeRateStateCopyWith<$Res> {
  _$ExchangeRateStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitialImplCopyWith<$Res> {
  factory _$$InitialImplCopyWith(
          _$InitialImpl value, $Res Function(_$InitialImpl) then) =
      __$$InitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialImplCopyWithImpl<$Res>
    extends _$ExchangeRateStateCopyWithImpl<$Res, _$InitialImpl>
    implements _$$InitialImplCopyWith<$Res> {
  __$$InitialImplCopyWithImpl(
      _$InitialImpl _value, $Res Function(_$InitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitialImpl implements _Initial {
  const _$InitialImpl();

  @override
  String toString() {
    return 'ExchangeRateState.initial()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<CurrencyEntity> currencyList) feched,
    required TResult Function() failed,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<CurrencyEntity> currencyList)? feched,
    TResult? Function()? failed,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<CurrencyEntity> currencyList)? feched,
    TResult Function()? failed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_LoadingExchangeRateState value) loading,
    required TResult Function(_FechedExchangeRateState value) feched,
    required TResult Function(_FailedExchangeRateState value) failed,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_LoadingExchangeRateState value)? loading,
    TResult? Function(_FechedExchangeRateState value)? feched,
    TResult? Function(_FailedExchangeRateState value)? failed,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_LoadingExchangeRateState value)? loading,
    TResult Function(_FechedExchangeRateState value)? feched,
    TResult Function(_FailedExchangeRateState value)? failed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements ExchangeRateState {
  const factory _Initial() = _$InitialImpl;
}

/// @nodoc
abstract class _$$LoadingExchangeRateStateImplCopyWith<$Res> {
  factory _$$LoadingExchangeRateStateImplCopyWith(
          _$LoadingExchangeRateStateImpl value,
          $Res Function(_$LoadingExchangeRateStateImpl) then) =
      __$$LoadingExchangeRateStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingExchangeRateStateImplCopyWithImpl<$Res>
    extends _$ExchangeRateStateCopyWithImpl<$Res,
        _$LoadingExchangeRateStateImpl>
    implements _$$LoadingExchangeRateStateImplCopyWith<$Res> {
  __$$LoadingExchangeRateStateImplCopyWithImpl(
      _$LoadingExchangeRateStateImpl _value,
      $Res Function(_$LoadingExchangeRateStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoadingExchangeRateStateImpl implements _LoadingExchangeRateState {
  const _$LoadingExchangeRateStateImpl();

  @override
  String toString() {
    return 'ExchangeRateState.loading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadingExchangeRateStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<CurrencyEntity> currencyList) feched,
    required TResult Function() failed,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<CurrencyEntity> currencyList)? feched,
    TResult? Function()? failed,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<CurrencyEntity> currencyList)? feched,
    TResult Function()? failed,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_LoadingExchangeRateState value) loading,
    required TResult Function(_FechedExchangeRateState value) feched,
    required TResult Function(_FailedExchangeRateState value) failed,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_LoadingExchangeRateState value)? loading,
    TResult? Function(_FechedExchangeRateState value)? feched,
    TResult? Function(_FailedExchangeRateState value)? failed,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_LoadingExchangeRateState value)? loading,
    TResult Function(_FechedExchangeRateState value)? feched,
    TResult Function(_FailedExchangeRateState value)? failed,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _LoadingExchangeRateState implements ExchangeRateState {
  const factory _LoadingExchangeRateState() = _$LoadingExchangeRateStateImpl;
}

/// @nodoc
abstract class _$$FechedExchangeRateStateImplCopyWith<$Res> {
  factory _$$FechedExchangeRateStateImplCopyWith(
          _$FechedExchangeRateStateImpl value,
          $Res Function(_$FechedExchangeRateStateImpl) then) =
      __$$FechedExchangeRateStateImplCopyWithImpl<$Res>;
  @useResult
  $Res call({List<CurrencyEntity> currencyList});
}

/// @nodoc
class __$$FechedExchangeRateStateImplCopyWithImpl<$Res>
    extends _$ExchangeRateStateCopyWithImpl<$Res, _$FechedExchangeRateStateImpl>
    implements _$$FechedExchangeRateStateImplCopyWith<$Res> {
  __$$FechedExchangeRateStateImplCopyWithImpl(
      _$FechedExchangeRateStateImpl _value,
      $Res Function(_$FechedExchangeRateStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? currencyList = null,
  }) {
    return _then(_$FechedExchangeRateStateImpl(
      currencyList: null == currencyList
          ? _value._currencyList
          : currencyList // ignore: cast_nullable_to_non_nullable
              as List<CurrencyEntity>,
    ));
  }
}

/// @nodoc

class _$FechedExchangeRateStateImpl implements _FechedExchangeRateState {
  const _$FechedExchangeRateStateImpl(
      {required final List<CurrencyEntity> currencyList})
      : _currencyList = currencyList;

  final List<CurrencyEntity> _currencyList;
  @override
  List<CurrencyEntity> get currencyList {
    if (_currencyList is EqualUnmodifiableListView) return _currencyList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_currencyList);
  }

  @override
  String toString() {
    return 'ExchangeRateState.feched(currencyList: $currencyList)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FechedExchangeRateStateImpl &&
            const DeepCollectionEquality()
                .equals(other._currencyList, _currencyList));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_currencyList));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FechedExchangeRateStateImplCopyWith<_$FechedExchangeRateStateImpl>
      get copyWith => __$$FechedExchangeRateStateImplCopyWithImpl<
          _$FechedExchangeRateStateImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<CurrencyEntity> currencyList) feched,
    required TResult Function() failed,
  }) {
    return feched(currencyList);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<CurrencyEntity> currencyList)? feched,
    TResult? Function()? failed,
  }) {
    return feched?.call(currencyList);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<CurrencyEntity> currencyList)? feched,
    TResult Function()? failed,
    required TResult orElse(),
  }) {
    if (feched != null) {
      return feched(currencyList);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_LoadingExchangeRateState value) loading,
    required TResult Function(_FechedExchangeRateState value) feched,
    required TResult Function(_FailedExchangeRateState value) failed,
  }) {
    return feched(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_LoadingExchangeRateState value)? loading,
    TResult? Function(_FechedExchangeRateState value)? feched,
    TResult? Function(_FailedExchangeRateState value)? failed,
  }) {
    return feched?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_LoadingExchangeRateState value)? loading,
    TResult Function(_FechedExchangeRateState value)? feched,
    TResult Function(_FailedExchangeRateState value)? failed,
    required TResult orElse(),
  }) {
    if (feched != null) {
      return feched(this);
    }
    return orElse();
  }
}

abstract class _FechedExchangeRateState implements ExchangeRateState {
  const factory _FechedExchangeRateState(
          {required final List<CurrencyEntity> currencyList}) =
      _$FechedExchangeRateStateImpl;

  List<CurrencyEntity> get currencyList;
  @JsonKey(ignore: true)
  _$$FechedExchangeRateStateImplCopyWith<_$FechedExchangeRateStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$FailedExchangeRateStateImplCopyWith<$Res> {
  factory _$$FailedExchangeRateStateImplCopyWith(
          _$FailedExchangeRateStateImpl value,
          $Res Function(_$FailedExchangeRateStateImpl) then) =
      __$$FailedExchangeRateStateImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$FailedExchangeRateStateImplCopyWithImpl<$Res>
    extends _$ExchangeRateStateCopyWithImpl<$Res, _$FailedExchangeRateStateImpl>
    implements _$$FailedExchangeRateStateImplCopyWith<$Res> {
  __$$FailedExchangeRateStateImplCopyWithImpl(
      _$FailedExchangeRateStateImpl _value,
      $Res Function(_$FailedExchangeRateStateImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$FailedExchangeRateStateImpl implements _FailedExchangeRateState {
  const _$FailedExchangeRateStateImpl();

  @override
  String toString() {
    return 'ExchangeRateState.failed()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FailedExchangeRateStateImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<CurrencyEntity> currencyList) feched,
    required TResult Function() failed,
  }) {
    return failed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(List<CurrencyEntity> currencyList)? feched,
    TResult? Function()? failed,
  }) {
    return failed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<CurrencyEntity> currencyList)? feched,
    TResult Function()? failed,
    required TResult orElse(),
  }) {
    if (failed != null) {
      return failed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_LoadingExchangeRateState value) loading,
    required TResult Function(_FechedExchangeRateState value) feched,
    required TResult Function(_FailedExchangeRateState value) failed,
  }) {
    return failed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_LoadingExchangeRateState value)? loading,
    TResult? Function(_FechedExchangeRateState value)? feched,
    TResult? Function(_FailedExchangeRateState value)? failed,
  }) {
    return failed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_LoadingExchangeRateState value)? loading,
    TResult Function(_FechedExchangeRateState value)? feched,
    TResult Function(_FailedExchangeRateState value)? failed,
    required TResult orElse(),
  }) {
    if (failed != null) {
      return failed(this);
    }
    return orElse();
  }
}

abstract class _FailedExchangeRateState implements ExchangeRateState {
  const factory _FailedExchangeRateState() = _$FailedExchangeRateStateImpl;
}
