part of 'exchange_rate_cubit.dart';

@freezed
class ExchangeRateState with _$ExchangeRateState {
  const factory ExchangeRateState.initial() = _Initial;

  const factory ExchangeRateState.loading() = _LoadingExchangeRateState;

  const factory ExchangeRateState.feched({
    required List<CurrencyEntity> currencyList,
  }) = _FechedExchangeRateState;

  const factory ExchangeRateState.failed() = _FailedExchangeRateState;
}
