import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:currency_exchange/data/models/response_models.dart/currency_entity.dart';
import 'package:currency_exchange/data/repositories/api/exchange_rate_repo.dart';
import 'package:currency_exchange/helpers/log_helper.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';

part 'exchange_rate_state.dart';
part 'exchange_rate_cubit.freezed.dart';

@singleton
class ExchangeRateCubit extends Cubit<ExchangeRateState> {
  ExchangeRateCubit(this._repo) : super(const ExchangeRateState.initial()) {
    getExchangeRate();
  }

  final ExchangeRateRepo _repo;

  FutureOr<void> getExchangeRate() async {
    emit(const _LoadingExchangeRateState());
    try {
      final formattedDate = DateFormat('yyyyMMdd').format(DateTime.now());
      final currencyList = await _repo.getExchangeRate(formattedDate);
      emit(_FechedExchangeRateState(currencyList: currencyList));
    } on Exception catch (e) {
      logger.e('error: $e');
      emit(const _FailedExchangeRateState());
    }
  }
}
