import 'package:country_icons/country_icons.dart';
import 'package:currency_exchange/config/constants/dimensions.dart';
import 'package:currency_exchange/data/models/response_models.dart/currency_entity.dart';
import 'package:flutter/material.dart';

class CurrencyCard extends StatelessWidget {
  const CurrencyCard({
    required this.currency,
    super.key,
  });

  final CurrencyEntity currency;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Center(
      child: SizedBox(
        width: MediaQuery.of(context).size.width < 550 ? double.infinity : 550,
        child: Card(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              if (currency.flag != null)
                SizedBox(
                  height: Dimensions.iconSideM,
                  width: Dimensions.iconSideM,
                  child: CountryIcons.getSvgFlag(currency.flag!),
                ),
              Text(currency.cc),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: theme.colorScheme.outline,
                    width: Dimensions.borderS,
                  ),
                  borderRadius: BorderRadius.circular(Dimensions.radiusM),
                ),
                padding: const EdgeInsets.all(Dimensions.paddingM),
                child: Text(
                  currency.rate.toStringAsFixed(2),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
