import 'package:currency_exchange/config/constants/dimensions.dart';
import 'package:currency_exchange/features/exchange_rate/cubit/exchange_rate_cubit.dart';
import 'package:currency_exchange/features/exchange_rate/view/widgets/currency_card.dart';
import 'package:currency_exchange/widgets/failed_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ExchangeRateScreen extends StatelessWidget {
  const ExchangeRateScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          locale.exchangeRateTitle,
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(Dimensions.paddingM),
          child: BlocBuilder<ExchangeRateCubit, ExchangeRateState>(
            builder: (context, state) => state.maybeWhen(
              feched: (currencyList) => ListView.builder(
                itemCount: currencyList.length,
                itemBuilder: (context, index) =>
                    CurrencyCard(currency: currencyList[index]),
              ),
              failed: () => const FailedWidget(),
              orElse: () => const Center(
                child: CircularProgressIndicator.adaptive(),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
