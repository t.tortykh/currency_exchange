import 'package:bloc/bloc.dart';
import 'package:currency_exchange/data/models/settings_model.dart';
import 'package:currency_exchange/data/services/local_storage_service.dart';
import 'package:flutter/material.dart';

class SettingsCubit extends Cubit<SettingsModel> {
  SettingsCubit() : super(SettingsModel.defaults()) {
    final settings = _localStorage.loadSettings();
    emitSettings(settings);
  }

  final LocalStorageService _localStorage = LocalStorageService.instance;

  Future<void> setLocale(Locale newLocale) async {
    await _localStorage.saveSettings(
      state.copyWith(
        locale: newLocale.languageCode,
      ),
    );
    emitSettings(_localStorage.loadSettings());
  }

  Future<void> switchMode(ThemeMode mode) async {
    await _localStorage.saveSettings(
      state.copyWith(
        themeMode: mode.name,
      ),
    );
    emitSettings(_localStorage.loadSettings());
  }

  void emitSettings(SettingsModel? newSettings) {
    if (newSettings != null) {
      emit(newSettings);
    }
  }
}
