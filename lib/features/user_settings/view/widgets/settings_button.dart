import 'package:currency_exchange/config/constants/dimensions.dart';
import 'package:flutter/material.dart';

class SettingsButton extends StatelessWidget {
  const SettingsButton({
    required this.title,
    required this.icon,
    required this.onTap,
    super.key,
  });

  final String title;
  final Widget icon;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Container(
      constraints: BoxConstraints(
        maxHeight:
            MediaQuery.of(context).size.height * Dimensions.screenPercentS,
        maxWidth:
            MediaQuery.of(context).size.height * Dimensions.screenPercentS,
      ),
      decoration: BoxDecoration(
        border: Border.all(width: Dimensions.borderM),
        color: theme.colorScheme.tertiary,
        borderRadius: const BorderRadius.all(
          Radius.circular(Dimensions.radiusM),
        ),
        boxShadow: [
          BoxShadow(
            color: theme.colorScheme.outline,
            blurRadius: Dimensions.radiusM,
            offset: const Offset(Dimensions.offsetM, Dimensions.offsetM),
          ),
        ],
      ),
      child: Stack(
        children: [
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  title,
                  style: theme.textTheme.bodyMedium,
                ),
                const SizedBox(height: Dimensions.gapL),
                SizedBox(
                  height: Dimensions.iconSideS,
                  width: Dimensions.iconSideM,
                  child: icon,
                ),
              ],
            ),
          ),
          Material(
            color: Colors.transparent,
            child: InkWell(
              borderRadius:
                  const BorderRadius.all(Radius.circular(Dimensions.radiusM)),
              onTap: onTap,
            ),
          ),
        ],
      ),
    );
  }
}
