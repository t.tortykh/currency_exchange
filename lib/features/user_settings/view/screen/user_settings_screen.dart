import 'package:country_icons/country_icons.dart';
import 'package:currency_exchange/data/models/settings_model.dart';
import 'package:currency_exchange/features/user_settings/cubit/settings_cubit.dart';
import 'package:currency_exchange/features/user_settings/view/widgets/settings_button.dart';
import 'package:currency_exchange/helpers/enum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class UserSettingsScreen extends StatelessWidget {
  const UserSettingsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(locale.settingsTitle),
      ),
      body: Center(
        child: BlocBuilder<SettingsCubit, SettingsModel>(
          builder: (context, settings) {
            final isDarkMode = settings.themeMode == ThemeMode.dark.name;
            final isUkr = locale.localeName == 'uk';
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                SettingsButton(
                  title: locale.themeMode,
                  icon: Icon(
                    isDarkMode ? Icons.light_mode : Icons.dark_mode,
                  ),
                  onTap: () => context.read<SettingsCubit>().switchMode(
                        isDarkMode ? ThemeMode.light : ThemeMode.dark,
                      ),
                ),
                SettingsButton(
                  title: locale.language,
                  icon: CountryIcons.getSvgFlag(
                    isUkr ? AppLanguage.ua.name : AppLanguage.gb.name,
                  ),
                  onTap: () => context.read<SettingsCubit>().setLocale(
                        isUkr ? const Locale('en') : const Locale('uk'),
                      ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
