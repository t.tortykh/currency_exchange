import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:intl/intl.dart';

part 'settings_model.freezed.dart';

@freezed
class SettingsModel with _$SettingsModel {
  const factory SettingsModel({
    required String locale,
    required String themeMode,
  }) = _SettingsModel;

  const SettingsModel._();

  factory SettingsModel.defaults() => SettingsModel(
        locale: Intl.systemLocale,
        themeMode: ThemeMode.light.toString(),
      );
}
