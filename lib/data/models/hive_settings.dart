import 'package:currency_exchange/data/models/hive_helper/hive_types.dart';
import 'package:currency_exchange/data/models/settings_model.dart';
import 'package:hive/hive.dart';

part 'hive_settings.g.dart';

@HiveType(typeId: HiveTypes.settingsModel, adapterName: 'SettingsAdapter')
class HiveSettings extends HiveObject {
  HiveSettings({
    required this.locale,
    required this.themeMode,
  });

  factory HiveSettings.fromSettingModel(SettingsModel model) => HiveSettings(
        locale: model.locale,
        themeMode: model.themeMode,
      );

  @HiveField(0)
  final String locale;
  @HiveField(1)
  final String themeMode;

  SettingsModel get model => SettingsModel(
        locale: locale,
        themeMode: themeMode,
      );
}
