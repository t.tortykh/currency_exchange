// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'currency_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CurrencyEntity _$CurrencyEntityFromJson(Map<String, dynamic> json) =>
    $checkedCreate(
      'CurrencyEntity',
      json,
      ($checkedConvert) {
        final val = CurrencyEntity(
          r030: $checkedConvert('r030', (v) => (v as num).toInt()),
          txt: $checkedConvert('txt', (v) => v as String),
          rate: $checkedConvert('rate', (v) => (v as num).toDouble()),
          cc: $checkedConvert('cc', (v) => v as String),
          exchangedate: $checkedConvert('exchangedate', (v) => v as String),
        );
        $checkedConvert('flag', (v) => val.flag = v as String?);
        return val;
      },
    );

Map<String, dynamic> _$CurrencyEntityToJson(CurrencyEntity instance) =>
    <String, dynamic>{
      'r030': instance.r030,
      'txt': instance.txt,
      'rate': instance.rate,
      'cc': instance.cc,
      'exchangedate': instance.exchangedate,
      'flag': instance.flag,
    };
