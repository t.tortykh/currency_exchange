// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'currency_entity.g.dart';

@JsonSerializable()
class CurrencyEntity extends Equatable {
  CurrencyEntity({
    required this.r030,
    required this.txt,
    required this.rate,
    required this.cc,
    required this.exchangedate,
  });

  factory CurrencyEntity.fromJson(Map<String, dynamic> json) =>
      _$CurrencyEntityFromJson(json);

  final int r030;
  final String txt;
  final double rate;
  final String cc;
  final String exchangedate;
  String? flag;

  Map<String, dynamic> toJson() => _$CurrencyEntityToJson(this);

  @override
  List<Object?> get props => [r030, txt, rate, cc, exchangedate, flag];
}
