// ignore_for_file: one_member_abstracts

import 'package:currency_exchange/api/client/currency_exchange_api_client.dart';
import 'package:currency_exchange/config/constants/injectable_environments.dart';
import 'package:currency_exchange/data/models/response_models.dart/currency_entity.dart';
import 'package:currency_exchange/helpers/enum.dart';
import 'package:injectable/injectable.dart';

abstract interface class ExchangeRateRepo {
  Future<List<CurrencyEntity>> getExchangeRate(String date);
}

@Injectable(as: ExchangeRateRepo, env: [CurrencyExchangeEnv.dev])
class ExchangeRateRepoImpl implements ExchangeRateRepo {
  ExchangeRateRepoImpl(this._client);

  final CurrencyExchangeApiClient _client;

  @override
  Future<List<CurrencyEntity>> getExchangeRate(String date) async {
    final resp = await _client.getExchangeRate(date: date);

    final currencies = Currency.values
        .map((currency) => currency.toString().split('.').last.toUpperCase())
        .toList();

    final results =
        resp.where((item) => currencies.contains(item.cc)).map((item) {
      item.flag = Currency.fromString(item.cc).countryCode;

      return item;
    }).toList();

    return results;
  }
}
