import 'package:currency_exchange/data/models/hive_settings.dart';
import 'package:currency_exchange/data/models/settings_model.dart';
import 'package:currency_exchange/helpers/log_helper.dart';
import 'package:flutter/foundation.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';

class LocalStorageService {
  LocalStorageService._();

  static final LocalStorageService _service = LocalStorageService._();

  static LocalStorageService get instance => _service;

  final String _settingsBoxName = 'settings';
  final String _settingsKey = 'currentSettings';
  late Box<HiveSettings> _settingsBox;

  Future<void> init() async {
    if (kIsWeb) {
      await Hive.initFlutter();
    } else {
      final appDocumentDirectory = await getApplicationDocumentsDirectory();
      await Hive.initFlutter(appDocumentDirectory.path);
    }

    Hive.registerAdapter(SettingsAdapter());

    _settingsBox = await Hive.openBox<HiveSettings>(_settingsBoxName);
    logger.i('Hive initiated');
  }

  Future<void> saveSettings(SettingsModel settings) async {
    await _settingsBox.put(
      _settingsKey,
      HiveSettings.fromSettingModel(settings),
    );
    await _settingsBox.flush();
  }

  SettingsModel? loadSettings() => _settingsBox.get(_settingsKey)?.model;

  Future<void> cleanCash() async {
    await _settingsBox.clear();
    logger.i('DB cleared');
  }
}
