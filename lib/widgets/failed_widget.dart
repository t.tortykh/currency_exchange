import 'package:currency_exchange/config/constants/dimensions.dart';
import 'package:currency_exchange/config/router/page_routes.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class FailedWidget extends StatelessWidget {
  const FailedWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Icon(
            Icons.warning_amber_rounded,
            size: Dimensions.iconSideM,
          ),
          Text(
            'Oops!',
            style: theme.textTheme.headlineMedium
                ?.copyWith(color: theme.colorScheme.secondary),
          ),
          Text(
            ' Please try again!',
            style: theme.textTheme.headlineSmall,
          ),
          const SizedBox(height: Dimensions.gapL),
          ElevatedButton(
            onPressed: () => context.go(AppNamedRoutes.home),
            child: const Text('Home'),
          ),
        ],
      ),
    );
  }
}
