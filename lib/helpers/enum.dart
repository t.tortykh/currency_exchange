import 'package:currency_exchange/helpers/log_helper.dart';

enum Currency {
  usd,
  eur,
  gbp,
  pln;

  String get countryCode {
    switch (this) {
      case Currency.usd:
        return 'us';
      case Currency.eur:
        return 'eu';
      case Currency.gbp:
        return 'gb';
      case Currency.pln:
        return 'pl';
    }
  }

  static Currency fromString(String code) {
    switch (code.toLowerCase()) {
      case 'usd':
        return Currency.usd;
      case 'eur':
        return Currency.eur;
      case 'pln':
        return Currency.pln;
      case 'gbp':
        return Currency.gbp;

      default:
        logger.i('Invalid currency code');
        return Currency.usd;
    }
  }
}

enum AppLanguage { gb, ua }
