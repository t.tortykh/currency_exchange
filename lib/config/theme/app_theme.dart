import 'package:currency_exchange/config/constants/app_colors.dart';
import 'package:currency_exchange/config/constants/app_theme_components.dart';
import 'package:flutter/material.dart';

final _themeComponents = AppThemeComponents();

ThemeData get lightTheme => ThemeData.light().copyWith(
      colorScheme: ColorScheme.fromSeed(
        seedColor: AppColors.seed,
      ),
      appBarTheme: _themeComponents.appBarLight,
      textTheme: _themeComponents.apptTextTheme,
      elevatedButtonTheme: _themeComponents.appElevatedButtonTheme,
      cardTheme: _themeComponents.appCardTheme,
    );

ThemeData get darkTheme => ThemeData.dark().copyWith(
      colorScheme: ColorScheme.fromSeed(
        seedColor: AppColors.seed,
      ),
      appBarTheme: _themeComponents.appBarDark,
      textTheme: _themeComponents.apptTextTheme,
      elevatedButtonTheme: _themeComponents.appElevatedButtonTheme,
      cardTheme: _themeComponents.appCardTheme,
    );
