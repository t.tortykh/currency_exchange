class AppRoutes {
  static const String home = '/';
  static const String exchangeRate = 'exchange_rate';
  static const String settings = 'favorite';
}

class AppNamedRoutes {
  static const String home = AppRoutes.home;
  static const String exchangeRate = '$home${AppRoutes.exchangeRate}';
  static const String settings = '$home${AppRoutes.settings}';
}
