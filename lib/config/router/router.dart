import 'package:currency_exchange/config/router/page_routes.dart';
import 'package:currency_exchange/core/di/service_locator.dart';
import 'package:currency_exchange/features/exchange_rate/cubit/exchange_rate_cubit.dart';
import 'package:currency_exchange/features/exchange_rate/view/screen/exchange_rate_screen.dart';
import 'package:currency_exchange/features/home/home_screen.dart';
import 'package:currency_exchange/features/user_settings/view/screen/user_settings_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

GoRouter router = GoRouter(
  debugLogDiagnostics: true,
  initialLocation: AppRoutes.home,
  routes: [
    GoRoute(
      name: AppNamedRoutes.home,
      path: AppRoutes.home,
      builder: (context, state) => const HomeScreen(),
      routes: [
        GoRoute(
          name: AppNamedRoutes.exchangeRate,
          path: AppRoutes.exchangeRate,
          builder: (context, state) => BlocProvider(
            create: (context) => sl<ExchangeRateCubit>(),
            child: const ExchangeRateScreen(),
          ),
        ),
        GoRoute(
          name: AppNamedRoutes.settings,
          path: AppRoutes.settings,
          builder: (context, state) => const UserSettingsScreen(),
        ),
      ],
    ),
  ],
);
