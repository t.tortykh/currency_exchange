import 'package:currency_exchange/config/constants/app_colors.dart';
import 'package:currency_exchange/config/constants/app_text_styles.dart';
import 'package:currency_exchange/config/constants/dimensions.dart';
import 'package:flutter/material.dart';

class AppThemeComponents {
  AppBarTheme get appBarLight => _getAppBar(AppColors.seed);

  AppBarTheme get appBarDark => _getAppBar(Colors.white);

  CardTheme get appCardTheme =>
      CardTheme(color: AppColors.seed.withOpacity(Dimensions.opacityL));

  AppBarTheme _getAppBar(Color color) => AppBarTheme(
        color: AppColors.seed.withOpacity(Dimensions.opacityL),
        elevation: Dimensions.elevationM,
        shadowColor: color,
        titleTextStyle: AppTextStyles.titleSmall.copyWith(
          color: Colors.white,
        ),
      );

  final apptTextTheme = const TextTheme().copyWith(
    headlineMedium: AppTextStyles.headlineMedium,
    headlineSmall: AppTextStyles.headlineSmall,
    titleSmall: AppTextStyles.titleSmall,
    bodyMedium: AppTextStyles.bodyMedium,
    bodySmall: AppTextStyles.bodySmall,
  );

  final appElevatedButtonTheme = ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      padding: const EdgeInsets.symmetric(
        vertical: Dimensions.paddingM,
        horizontal: Dimensions.paddingL,
      ),
      foregroundColor: Colors.white,
      textStyle: AppTextStyles.titleSmall,
      backgroundColor: AppColors.seed,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(Dimensions.radiusM),
        ),
      ),
    ),
  );
}
