mixin Dimensions {
  //padding
  static const double paddingL = 32;
  static const double paddingM = 16;

  //radiuses
  static const double radiusM = 15;

  //size boxes
  static const double gapXS = 5;
  static const double gapM = 15;
  static const double gapL = 30;

  //elevation
  static const double elevationM = 10;

  //icons
  static const double iconSideS = 50;
  static const double iconSideM = 80;

  //opacity
  static const double opacityS = 0.2;
  static const double opacityL = 0.7;

  //border
  static const double borderS = 2;
  static const double borderM = 4;

  //screenPercent
  static const double screenPercentS = 0.2;

  //offset
  static const double offsetM = 4;
}
