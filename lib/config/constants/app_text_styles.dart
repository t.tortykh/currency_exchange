import 'package:flutter/material.dart';

class AppTextStyles {
  static const _mainFontFamily = 'Quicksand';

  static const headlineMedium = TextStyle(
    fontFamily: _mainFontFamily,
    fontSize: 70,
    fontWeight: FontWeight.w700,
  );

  static const headlineSmall = TextStyle(
    fontFamily: _mainFontFamily,
    fontSize: 35,
    fontWeight: FontWeight.w500,
  );

  static const titleSmall = TextStyle(
    fontFamily: _mainFontFamily,
    fontSize: 22,
    fontWeight: FontWeight.w700,
  );

  static const bodyMedium = TextStyle(
    fontFamily: _mainFontFamily,
    fontSize: 16,
    fontWeight: FontWeight.w600,
  );

  static const bodySmall = TextStyle(
    fontFamily: _mainFontFamily,
    fontSize: 14,
    fontWeight: FontWeight.w400,
  );
}
