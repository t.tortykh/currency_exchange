import 'dart:async';

import 'package:currency_exchange/data/models/response_models.dart/currency_entity.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

part 'currency_exchange_api_client.g.dart';

@RestApi()
abstract class CurrencyExchangeApiClient {
  factory CurrencyExchangeApiClient(
    Dio dio, {
    String baseUrl,
  }) = _CurrencyExchangeApiClient;
  static const String _type = 'json';
  static const String _currencyExchange = '/exchangenew';

  @GET('$_currencyExchange?$_type&date={date}')
  Future<List<CurrencyEntity>> getExchangeRate({
    @Path('date') required String date,
  });
}
