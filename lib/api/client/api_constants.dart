// ignore_for_file: avoid_classes_with_only_static_members
import 'package:flutter_dotenv/flutter_dotenv.dart';

abstract class ApiConstants {
  static final _baseUrl = dotenv.env['API_URL']!;
  static String get baseUrl => _baseUrl;
  static String get baseDomain => _baseUrl;
}
